# OpenML dataset: yeast-1_vs_7

https://www.openml.org/d/41322

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

A imbalanced version of the Yeast data set. where the possitive examples belong to class VAC and the negative examples belong to classes NUC. Moreover. Pox attribute has been removed in this data set. because every instance in this data set had a value of 0.0 for this attribute. (IR: 13.87)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41322) of an [OpenML dataset](https://www.openml.org/d/41322). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41322/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41322/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41322/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

